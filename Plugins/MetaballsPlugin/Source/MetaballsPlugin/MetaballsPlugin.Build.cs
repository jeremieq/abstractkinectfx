using UnrealBuildTool;
using System.IO;

public class MetaballsPlugin : ModuleRules
{

    public MetaballsPlugin(ReadOnlyTargetRules Target) : base(Target)
    {
        PrivateIncludePaths.AddRange(new string[] { "MetaballsPlugin/Private" });
	    PublicIncludePaths.AddRange(new string[] { "MetaballsPlugin/Public" });

        PublicDependencyModuleNames.AddRange(new string[] { "Engine", "Core", "CoreUObject", "InputCore", "ProceduralMeshComponent" });
    }
}