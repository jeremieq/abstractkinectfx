// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "KinectRTXGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class KINECTRTX_API AKinectRTXGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
